import pytest
from flask import Flask

import urlinfo.exceptions as exc
import urlinfo.lookup_service
from test_utils import NORMAL_URLS
from urlinfo import update_api as api
from urlinfo.collections import AttrDict as d
from urlinfo.response_codes import (BAD_REQUEST, DATABASE_UNAVAILABLE, OK,
                                    UNKNOWN_ERROR)


@pytest.fixture
def app():
    test_app = Flask(__name__)
    test_app.register_blueprint(api.update, url_prefix='/update/1')
    return test_app


def test_add_urls_calls_lookup_service(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK

    req = d(urls=[u[0] for u in NORMAL_URLS])
    with app.test_request_context(json=req):
        api.add_urls()
    urlinfo.lookup_service.add_urls.called_once_with(req.urls)


def test_add_urls_response(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK
    expected = d(result=OK.id, result_code=OK.code)
    req = d(urls=[u[0] for u in NORMAL_URLS])
    with app.test_request_context(json=req):
        resp = api.add_urls()

    assert resp.status_code == 200
    assert resp.json.items() >= expected.items()


def test_add_urls_no_request_data(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK

    with app.test_request_context():
        with pytest.raises(exc.BadRequestError) as excinfo:
            api.add_urls()
        assert excinfo.value.response_code == BAD_REQUEST


def test_add_urls_invalid_request_data(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK

    with app.test_request_context(data='Not JSON'):
        with pytest.raises(exc.BadRequestError) as excinfo:
            api.add_urls()
        assert excinfo.value.response_code == BAD_REQUEST


def test_add_urls_missing_urls(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK

    with app.test_request_context(json=d(foo=1, bar=2)):
        with pytest.raises(exc.BadRequestError) as excinfo:
            api.add_urls()
        assert excinfo.value.response_code == BAD_REQUEST


def test_add_urls_extra_keys(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK

    with app.test_request_context(json=d(urls=['host/path'], extra=1)):
        with pytest.raises(exc.BadRequestError) as excinfo:
            api.add_urls()
        assert excinfo.value.response_code == BAD_REQUEST


def test_add_urls_service_error_raised(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    err = exc.ServiceError(DATABASE_UNAVAILABLE)
    urlinfo.lookup_service.add_urls.side_effect = err

    with app.test_request_context(json=d(urls=['host/path'])):
        with pytest.raises(exc.ServiceError) as excinfo:
            api.add_urls()
    assert excinfo.value.response_code == DATABASE_UNAVAILABLE


def test_add_urls_unknown_error_raised(mocker, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.side_effect = ValueError

    with app.test_request_context(json=d(urls=['host/path'])):
        with pytest.raises(exc.ServiceError) as excinfo:
            api.add_urls()
    assert excinfo.value.response_code == UNKNOWN_ERROR
