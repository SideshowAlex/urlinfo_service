import pytest
from flask import Flask

import urlinfo.exceptions as exc
import urlinfo.lookup_service
from urlinfo import urlinfo_api as api
from urlinfo.response_codes import (DATABASE_UNAVAILABLE, OK, SAFE,
                                    UNKNOWN_ERROR, UNSAFE)


@pytest.fixture
def app():
    test_app = Flask(__name__)
    test_app.register_blueprint(api.urlinfo, url_prefix='/urlinfo/1')
    return test_app


def test_check_url_calls_lookup_service(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = SAFE

    api.check('host/path1/path2')
    urlinfo.lookup_service.check_url.called_once_with('host/path1/path2')


def test_check_url_safe(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = SAFE

    res = api.check('host/path1/path2')
    assert res.status_code == 200
    assert res.json == {'result': 'SAFE', 'result_code': 101}


def test_check_url_unsafe(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = UNSAFE

    res = api.check('host/path1/path2')
    assert res.status_code == 200
    assert res.json == {'result': 'UNSAFE', 'result_code': 102}


def test_check_url_database_error(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    db_error = exc.ServiceError(DATABASE_UNAVAILABLE, 'Database error')
    urlinfo.lookup_service.check_url.side_effect = db_error

    with pytest.raises(exc.ServiceError) as excinfo:
        api.check('host/path1/path2')
    assert excinfo.value.response_code == DATABASE_UNAVAILABLE


def test_check_url_unknown_error(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.side_effect = ValueError

    with pytest.raises(exc.ServiceError) as excinfo:
        api.check('host/path1/path2')
    assert excinfo.value.response_code == UNKNOWN_ERROR


def test_add_url(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    urlinfo.lookup_service.add_url.return_value = OK

    res = api.add('host/path1/path2')
    assert res.status_code == 200
    assert res.json == {'result': 'OK', 'result_code': 100}


def test_add_url_database_error(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    db_error = exc.ServiceError(DATABASE_UNAVAILABLE, 'Database error')
    urlinfo.lookup_service.add_url.side_effect = db_error

    with pytest.raises(exc.ServiceError) as excinfo:
        api.add('host/path1/path2')
    assert excinfo.value.response_code == DATABASE_UNAVAILABLE


def test_add_url_unknown_error(mocker, client, app):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    urlinfo.lookup_service.add_url.side_effect = ValueError

    with pytest.raises(exc.ServiceError) as excinfo:
        api.add('host/path1/path2')
    assert excinfo.value.response_code == UNKNOWN_ERROR
