from itertools import chain

NORMAL_URLS = [
    ('host', ('host', '')),
    ('host/', ('host', '/')),
    ('host/path', ('host', '/path')),
    ('host/path/', ('host', '/path/')),
    ('host/path1/path2', ('host', '/path1/path2')),
    ('host/path1/path2/', ('host', '/path1/path2/')),
    ('host:8080/path', ('host:8080', '/path')),
]

UNNORMALIZED_PATH_URLS = [
    ('host//path', ('host', '/path')),
    ('host/path1//path2', ('host', '/path1/path2')),
    ('host/path1/path2//', ('host', '/path1/path2/')),
    ('host//path1//path2//', ('host', '/path1/path2/')),
]

UNNORMALIZED_HOST_URLS = [
    ('host:80/path', ('host', '/path')),
    ('host:443/path', ('host', '/path')),
]

ALL_URLS = list(
    chain(NORMAL_URLS, UNNORMALIZED_HOST_URLS, UNNORMALIZED_PATH_URLS)
)
