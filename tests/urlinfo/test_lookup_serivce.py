from itertools import chain
from unittest.mock import MagicMock

import pytest
from redis.exceptions import ConnectionError, TimeoutError

import urlinfo
import urlinfo.exceptions as exc
from test_utils import (ALL_URLS, NORMAL_URLS, UNNORMALIZED_HOST_URLS,
                        UNNORMALIZED_PATH_URLS)
from urlinfo import lookup_service
from urlinfo.lookup_service import add_url, add_urls, check_url, split_url
from urlinfo.response_codes import (DATABASE_UNAVAILABLE, OK, SAFE,
                                    UNKNOWN_ERROR, UNSAFE)


@pytest.fixture
def mock_redis(mocker):
    mocker.patch.object(urlinfo.redis, 'init')
    urlinfo.redis.init.return_value = MagicMock()
    return urlinfo.services.redis


@pytest.mark.parametrize('url,expected', NORMAL_URLS)
def test_split_url(url, expected):
    actual = split_url(url)
    assert expected == actual


@pytest.mark.parametrize('url,expected', UNNORMALIZED_PATH_URLS)
def test_split_url_normalize_path(url, expected):
    actual = split_url(url)
    assert expected == actual


@pytest.mark.parametrize('url,expected', UNNORMALIZED_HOST_URLS)
def test_split_url_normalize_host_port(url, expected):
    actual = split_url(url)
    assert expected == actual


@pytest.mark.parametrize('url,expected', ALL_URLS)
def test_check_url_unsafe(mock_redis, url, expected):
    mock_redis.hexists.return_value = 1
    res = check_url(url)

    assert res == UNSAFE
    assert mock_redis.hexists.called_once_with(*expected)


@pytest.mark.parametrize('url,expected', ALL_URLS)
def test_check_url_safe(mock_redis, url, expected):
    mock_redis.hexists.return_value = 0
    res = check_url(url)

    assert res == SAFE
    assert mock_redis.hexists.called_once_with(*expected)


def test_check_url_connection_error(mock_redis):
    mock_redis.hexists.side_effect = ConnectionError

    with pytest.raises(exc.ServiceError) as excinfo:
        check_url('host/path1/path2')

    ex = excinfo.value
    assert ex.response_code == DATABASE_UNAVAILABLE


def test_check_url_unknown_error(mock_redis):
    mock_redis.hexists.side_effect = ValueError

    with pytest.raises(exc.ServiceError) as excinfo:
        check_url('host/path1/path2')

    ex = excinfo.value
    assert ex.response_code == UNKNOWN_ERROR


@pytest.mark.parametrize('url,expected', ALL_URLS)
def test_add_url(mock_redis, url, expected):
    mock_redis.hset.return_value = 0
    res = add_url(url)

    assert res == OK
    assert mock_redis.hset.called_once_with(*expected)


def test_add_url_connection_error(mock_redis):
    mock_redis.hset.side_effect = ConnectionError

    with pytest.raises(exc.ServiceError) as excinfo:
        add_url('host/path1/path2')

    ex = excinfo.value
    assert ex.response_code == DATABASE_UNAVAILABLE


def test_add_url_unknown_error(mock_redis):
    mock_redis.hset.side_effect = ValueError

    with pytest.raises(exc.ServiceError) as excinfo:
        add_url('host/path1/path2')

    ex = excinfo.value
    assert ex.response_code == UNKNOWN_ERROR


def test_update_urls(mocker):
    mocker.patch.object(lookup_service, 'add_url', return_value=OK)

    urls = [u[0] for u in NORMAL_URLS]
    res = add_urls(urls)

    assert lookup_service.add_url.call_count == len(urls)

    for idx, url in enumerate(urls):
        call_url = lookup_service.add_url.call_args_list[idx][0][0]
        assert url == call_url

    assert res == OK


def test_update_urls_with_failures(mock_redis):
    def hset(host_port, *_):
        if host_port == 'timeout_error_host':
            raise TimeoutError
        elif host_port == 'value_error_host':
            raise ValueError
        return 1

    mock_redis.hset.side_effect = hset

    good_urls = [
        'good_host/path1',
        'good_host/path2',
        'good_host/path3',
        'good_host/path4',
    ]
    timeout_urls = [
        'timeout_error_host/path1',
        'timeout_error_host/path2',
    ]
    value_error_urls = [
        'value_error_host/path1',
        'value_error_host/path2',
    ]

    with pytest.raises(exc.UpdateFailedError) as excinfo:
        add_urls(chain(good_urls, timeout_urls, value_error_urls))

    failures = excinfo.value.failures
    assert len(failures) == 4

    def find_failure(url):
        return next((f for f in failures if f.url == url), None)

    assert all(find_failure(u) is None for u in good_urls)
    assert all(find_failure(u).response_code == DATABASE_UNAVAILABLE
               for u in timeout_urls)
    assert all(find_failure(u).response_code == UNKNOWN_ERROR
               for u in value_error_urls)
