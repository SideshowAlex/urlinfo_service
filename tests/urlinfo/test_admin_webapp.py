import pytest
from flask import Flask, url_for

import urlinfo.exceptions as exc
import urlinfo.lookup_service
from test_utils import NORMAL_URLS
from urlinfo import update_api
from urlinfo.collections import AttrDict as d
from urlinfo.response_codes import (BAD_REQUEST, DATABASE_UNAVAILABLE, OK,
                                    UNKNOWN_ERROR, UPDATE_FAILED)


@pytest.fixture
def app():
    test_app = Flask(__name__)
    test_app.register_blueprint(update_api.update, url_prefix='/update/1')
    return test_app


@pytest.fixture
def client_post(client, app):
    def on_call(endpoint, json=None, data=None):
        ep_url = url_for(endpoint)
        args = d()
        if json is not None:
            args.json = json
        elif data is not None:
            args.data = data
        return client.post(ep_url, **args)

    return on_call


def test_add_urls_calls_lookup_service(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK
    req = d(urls=[u[0] for u in NORMAL_URLS])
    client_post('update.add_urls', json=req)

    urlinfo.lookup_service.add_urls.assert_called_once_with(req.urls)


def test_add_urls_response(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.return_value = OK
    expected_resp = d(result=OK.id, result_code=OK.code)
    req = d(urls=[u[0] for u in NORMAL_URLS])
    res = client_post('update.add_urls', json=req)

    assert res.status_code == 200
    assert res.json.items() >= expected_resp.items()


def test_add_urls_failed_to_update(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    mocked_failures = [
        d(url='host1/path', response_code=DATABASE_UNAVAILABLE),
        d(url='host2/path', response_code=UNKNOWN_ERROR),
    ]
    err = exc.UpdateFailedError(mocked_failures)
    urlinfo.lookup_service.add_urls.side_effect = err

    failures = [
        d(url=f.url, result=f.response_code.id,
          result_code=f.response_code.code)
        for f in mocked_failures
    ]
    expected_resp = d(result=UPDATE_FAILED.id, result_code=UPDATE_FAILED.code)
    req = d(urls=[u[0] for u in NORMAL_URLS])
    res = client_post('update.add_urls', json=req)

    assert res.status_code == 409
    assert res.json.items() >= expected_resp.items()

    assert all(expected == actual for (expected, actual) in
               zip(failures, res.json['failures']))


def test_add_urls_no_request_data(client_post):
    expected_resp = d(result=BAD_REQUEST.id, result_code=BAD_REQUEST.code)
    res = client_post('update.add_urls')
    assert res.status_code == 400
    assert res.json.items() > expected_resp.items()


def test_add_urls_invalid_request_data(client_post):
    expected_resp = d(result=BAD_REQUEST.id, result_code=BAD_REQUEST.code)
    res = client_post('update.add_urls', data='Not JSON')
    assert res.status_code == 400
    assert res.json.items() > expected_resp.items()


def test_add_urls_missing_urls(client_post):
    expected_resp = d(result=BAD_REQUEST.id, result_code=BAD_REQUEST.code)
    res = client_post('update.add_urls', json=d(foo=1, bar=2))
    assert res.status_code == 400
    assert res.json.items() > expected_resp.items()


def test_add_urls_extra_keys(client_post):
    expected_resp = d(result=BAD_REQUEST.id, result_code=BAD_REQUEST.code)
    body = d(urls='host/path', extra=1)
    res = client_post('update.add_urls', json=body)
    assert res.status_code == 400
    assert res.json.items() > expected_resp.items()


def test_add_urls_service_error_raised(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    err = exc.ServiceError(DATABASE_UNAVAILABLE)
    urlinfo.lookup_service.add_urls.side_effect = err
    req = d(urls=[u[0] for u in NORMAL_URLS])
    expected = d(result=DATABASE_UNAVAILABLE.id,
                 result_code=DATABASE_UNAVAILABLE.code)
    res = client_post('update.add_urls', json=req)

    assert res.status_code == 500
    assert res.json.items() >= expected.items()


def test_add_urls_unexpected_error_raised(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_urls')
    urlinfo.lookup_service.add_urls.side_effect = ValueError
    req = d(urls=[u[0] for u in NORMAL_URLS])
    expected = d(result=UNKNOWN_ERROR.id,
                 result_code=UNKNOWN_ERROR.code)
    res = client_post('update.add_urls', json=req)

    assert res.status_code == 500
    assert res.json.items() >= expected.items()
