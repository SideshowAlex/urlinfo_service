import pytest
from flask import Flask, url_for

import urlinfo.exceptions as exc
import urlinfo.lookup_service
from test_utils import ALL_URLS
from urlinfo import urlinfo_api
from urlinfo.response_codes import DATABASE_UNAVAILABLE, OK, SAFE, UNSAFE


@pytest.fixture
def app():
    test_app = Flask(__name__)
    test_app.register_blueprint(urlinfo_api.urlinfo, url_prefix='/urlinfo/1')
    return test_app


@pytest.fixture
def client_get(client, app):
    def on_call(endpoint, url, **query):
        if query is None:
            query = {}

        ep_url = url_for(endpoint, url=url, **query)
        return client.get(ep_url)
    return on_call


@pytest.fixture
def client_post(client, app):
    def on_call(endpoint, url, **query):
        if query is None:
            query = {}

        ep_url = url_for(endpoint, url=url, **query)
        return client.post(ep_url)
    return on_call


@pytest.mark.parametrize('url', (s[0] for s in ALL_URLS))
def test_check_url_api_calls_lookup_service(mocker, client_get, url):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = SAFE
    client_get('urlinfo.check', url)

    urlinfo.lookup_service.check_url.assert_called_once_with(url)


def test_check_url_safe(mocker, client_get):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = SAFE
    url = 'host_port/path1/path2'
    res = client_get('urlinfo.check', url)

    assert res.status_code == 200
    assert res.json == {'result': 'SAFE', 'result_code': 101}


def test_check_url_unsafe(mocker, client_get):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.return_value = UNSAFE
    url = 'host_port/path1/path2'
    res = client_get('urlinfo.check', url)

    assert res.status_code == 200
    assert res.json == {'result': 'UNSAFE', 'result_code': 102}


def test_check_url_database_error(mocker, client_get):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    db_error = exc.ServiceError(DATABASE_UNAVAILABLE, 'Database error')
    urlinfo.lookup_service.check_url.side_effect = db_error
    url = 'host_port/path1/path2'
    res = client_get('urlinfo.check', url)

    expected = {'result': 'DATABASE_UNAVAILABLE', 'result_code': 1001}

    assert res.status_code == 500
    assert res.json.items() >= expected.items()


def test_check_url_unknown_error(mocker, client_get):
    mocker.patch.object(urlinfo.lookup_service, 'check_url')
    urlinfo.lookup_service.check_url.side_effect = ValueError
    url = 'host_port/path1/path2'
    res = client_get('urlinfo.check', url)

    expected = {'result': 'UNKNOWN_ERROR', 'result_code': 1000}

    assert res.status_code == 500
    assert res.json.items() >= expected.items()


@pytest.mark.parametrize('url', (s[0] for s in ALL_URLS))
def test_add_url_api_calls_lookup_service(mocker, client_post, url):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    urlinfo.lookup_service.add_url.return_value = SAFE
    client_post('urlinfo.add', url)

    urlinfo.lookup_service.add_url.assert_called_once_with(url)


def test_add_url(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    urlinfo.lookup_service.add_url.return_value = OK
    url = 'host_port/path1/path2'
    res = client_post('urlinfo.add', url)

    assert res.status_code == 200
    assert res.json == {'result': 'OK', 'result_code': 100}


def test_add_url_database_error(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    db_error = exc.ServiceError(DATABASE_UNAVAILABLE, 'Database error')
    urlinfo.lookup_service.add_url.side_effect = db_error
    url = 'host_port/path1/path2'
    res = client_post('urlinfo.add', url)

    expected = {'result': 'DATABASE_UNAVAILABLE', 'result_code': 1001}

    assert res.status_code == 500
    assert res.json.items() >= expected.items()


def test_add_url_unknown_error(mocker, client_post):
    mocker.patch.object(urlinfo.lookup_service, 'add_url')
    urlinfo.lookup_service.add_url.side_effect = ValueError
    url = 'host_port/path1/path2'
    res = client_post('urlinfo.add', url)

    expected = {'result': 'UNKNOWN_ERROR', 'result_code': 1000}

    assert res.status_code == 500
    assert res.json.items() >= expected.items()
