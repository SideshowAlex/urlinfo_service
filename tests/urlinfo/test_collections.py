from urlinfo.collections import AttrDict


def test_AttrDict_getattr():
    target = AttrDict()
    target.x = 'foo'
    target.y = 'bar'

    assert target['x'] == 'foo'
    assert target['y'] == 'bar'


def test_AttrDict_setattr():
    target = AttrDict()
    target['x'] = 'foo'
    target['y'] = 'bar'

    assert target.x == 'foo'
    assert target.y == 'bar'


def test_AttrDict_dict_method():
    target = AttrDict()
    target.x = 'foo'
    target.y = 'bar'
    keys = target.keys()

    assert all(key in keys for key in ('x', 'y'))
