import pytest

from urlinfo import yaml
from urlinfo.collections import AttrDict


@pytest.fixture
def sample_config():
    return """
string: abc123
int: 1
bool: True
array:
  - abc
  - def
  - ghi
mapping:
  string: def456
  int: 2
  bool: False
  array:
    - ijk
    - lmn
  mapping:
    foo: 1
    bar: 2
"""


def test_loader(sample_config):
    loaded = yaml.load(sample_config)
    assert isinstance(loaded, AttrDict)
    assert all(
        key in loaded for key in ('string', 'int', 'bool', 'array', 'mapping')
    )
