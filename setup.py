from setuptools import setup

setup(
    setup_requires=['pbr', 'tox'],
    pbr=True,
)
