import logging

from redis.exceptions import ConnectionError, TimeoutError

import urlinfo
import urlinfo.exceptions as exc
from urlinfo.collections import AttrDict as d
from urlinfo.response_codes import (DATABASE_UNAVAILABLE, OK, SAFE,
                                    UNKNOWN_ERROR, UNSAFE)

log = logging.getLogger(__name__)

services = urlinfo.services


def _normalize_host_port(host_port):
    """
    Strips the port off of a host_port if the port is a default HTTP/HTTPS
    port.
    """
    try:
        host, port = host_port.rsplit(':', 1)
    except ValueError:
        return host_port
    if port == '80' or port == '443':
        return host
    return host_port


def _normalize_path(path):
    """
    Replaces repeated slashes in url path with a single slash`
    :param path:
    :return:
    """
    if path is None:
        return ''
    if not path:
        return '/'
    normalized_path = '/'.join(s for s in path.split('/') if s)

    if path[-1] == '/':
        return f'/{normalized_path}/'
    return f'/{normalized_path}'


def split_url(url):
    try:
        host_port, path = url.split('/', 1)
    except ValueError:
        host_port, path = url, None

    host_port = _normalize_host_port(host_port)
    path = _normalize_path(path)
    return host_port, path


def check_url(url):
    """
    Check redis to see if there is an entry in for the host_port/path.
    The port is removed from the host_port if it the default HTTP/HTTPS
    port is used.
    """
    log.debug(f'Checking {url}')
    host_port, path = split_url(url)

    try:
        is_safe = not services.redis.hexists(host_port, path)
    except (ConnectionError, TimeoutError) as e:
        log.error(str(e))
        raise exc.ServiceError(DATABASE_UNAVAILABLE, str(e))
    except Exception as e:
        log.exception(str(e))
        raise exc.ServiceError(UNKNOWN_ERROR, str(e))
    else:
        return SAFE if is_safe else UNSAFE


def add_url(url):
    """
    Marks a host_port/path in redis to denote that it is unsafe.
    The port is removed from the host_port if it the default HTTP/HTTPS
    port is used.
    """
    log.debug(f'adding {url}')
    host_port, path = split_url(url)

    try:
        services.redis.hset(host_port, path, 1)
    except (ConnectionError, TimeoutError) as e:
        log.error(str(e))
        raise exc.ServiceError(DATABASE_UNAVAILABLE, str(e))
    except Exception as e:
        log.exception(str(e))
        raise exc.ServiceError(UNKNOWN_ERROR, str(e))
    else:
        return OK


def add_urls(urls):
    """
    Loads a list of URLs into the redis database. All urls submitted will be
    attempted to be added to redis. A UpdateFailedError will be raised
    containing an list of all failed updates and the error response code and
    reason of the failure
    """
    log.debug("Adding URLs")

    failures = []
    for url in urls:
        try:
            add_url(url)
        except exc.ServiceError as e:
            log.error(f'Error adding {url}: {e}')
            failure_info = d(url=url, response_code=e.response_code, msg=e.msg)
            failures.append(failure_info)

    if failures:
        raise exc.UpdateFailedError(failures)
    return OK
