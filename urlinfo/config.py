import os

from urlinfo import yaml

CONFIG_ENVVAR = 'CONFIG_FILE'
CONFIG_DEFAULT = 'config.yaml'


def init(config_file=None):
    if not config_file:
        config_file = os.environ.get(CONFIG_ENVVAR, CONFIG_DEFAULT)

    with open(config_file) as stream:
        return yaml.load(stream)
