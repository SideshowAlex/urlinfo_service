import urlinfo.config
import urlinfo.logging
import urlinfo.redis


class Services:
    def __init__(self):
        self.config = urlinfo.config.init()
        self.logging = urlinfo.logging.init(self.config.logging)
        self._redis = None

    @property
    def redis(self):
        if not self._redis:
            self._redis = urlinfo.redis.init(self.config.redis)
        return self._redis


services = Services()
