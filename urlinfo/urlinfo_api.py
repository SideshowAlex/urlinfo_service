import logging

from flask import Blueprint

import urlinfo.exceptions as exc
from urlinfo import lookup_service
from urlinfo.collections import AttrDict as d
from urlinfo.response_codes import UNKNOWN_ERROR
from urlinfo.utils import json_resp

log = logging.getLogger(__name__)

urlinfo = Blueprint('urlinfo', __name__)


@urlinfo.route('/<path:url>', methods=['GET'])
def check(url):
    log.debug(f'Request made for {url}')
    # Note that the query string is stripped from the path, and is not
    # included in this check.
    try:
        result = lookup_service.check_url(url)
    except exc.ServiceError as e:
        log.error(e)
        raise e
    except Exception as e:
        log.exception(e)
        raise exc.ServiceError(UNKNOWN_ERROR)
    else:
        return json_resp(result)


@urlinfo.route('/<path:url>', methods=['POST'])
def add(url):
    log.debug(f'Request made for {url}')
    # Note that the query string is stripped from the path, and is not
    # included in this check.
    try:
        result = lookup_service.add_url(url)
        return json_resp(result)
    except exc.ServiceError as e:
        log.error(e)
        raise e
    except Exception as e:
        log.exception(e)
        raise exc.ServiceError(UNKNOWN_ERROR)


@urlinfo.errorhandler(exc.ServiceError)
def handle_service_error(error):
    context = d()
    if error.msg:
        context.reason = error.msg
    return json_resp(error.response_code, status_code=500, **context)
