import logging

from flask import Flask

import urlinfo
from urlinfo import update_api, urlinfo_api

log = logging.getLogger(__name__)

config = urlinfo.services.config


def main():
    webapp_config = config.urlinfo_webapp

    log.info("Starting urlinfo_webapp")
    app = Flask(__name__)
    log.debug("Registering urlinfo_api")
    app.register_blueprint(urlinfo_api.urlinfo, url_prefix='/urlinfo/1')
    log.debug("Registering update_api")
    app.register_blueprint(update_api.update, url_prefix='/update/1')
    app.run(host='0.0.0.0', port=webapp_config.port)


if __name__ == '__main__':
    main()
