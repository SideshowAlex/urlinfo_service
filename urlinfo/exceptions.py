from urlinfo.response_codes import BAD_REQUEST, UPDATE_FAILED


class ServiceError(Exception):
    def __init__(self, response_code, msg=None):
        self.response_code = response_code
        self.msg = msg


class UpdateFailedError(ServiceError):
    def __init__(self, failures, msg=None):
        super().__init__(UPDATE_FAILED, msg=msg)
        self.failures = failures


class BadRequestError(ServiceError):
    def __init__(self, msg=None):
        super().__init__(BAD_REQUEST, msg=msg)
