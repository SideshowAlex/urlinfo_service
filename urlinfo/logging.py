import logging


def init(config):
    logging_format = '%(asctime)s - [%(levelname)s] %(name)s - %(message)s'
    formatter = logging.Formatter(logging_format)
    ch = logging.StreamHandler()
    ch.setLevel(config.log_level)
    ch.setFormatter(formatter)

    ch.setLevel(config.log_level)
    ch.setFormatter(formatter)

    for package in '__main__', 'urlinfo':
        logger = logging.getLogger(package)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(ch)
