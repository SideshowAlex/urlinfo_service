import logging

from redis import Redis

log = logging.getLogger(__name__)

DEFAULT_PORT = 6379


def init(config):
    host = config.host
    port = config.get('port', DEFAULT_PORT)
    log.debug(f'Connecting to redis client {host}:{port}')
    return Redis(host=host, port=port)
