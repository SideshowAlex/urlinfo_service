import yaml

from urlinfo.collections import AttrDict


def load(stream, Loader=yaml.Loader, object_pairs_hook=AttrDict):

    class AttrLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))

    AttrLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)

    return yaml.load(stream, AttrLoader)
