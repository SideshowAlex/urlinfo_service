from collections import namedtuple

ResponseCode = namedtuple('ResponseCode', 'code, id')

OK = ResponseCode(100, 'OK')
SAFE = ResponseCode(101, 'SAFE')
UNSAFE = ResponseCode(102, 'UNSAFE')

UNKNOWN_ERROR = ResponseCode(1000, 'UNKNOWN_ERROR')
DATABASE_UNAVAILABLE = ResponseCode(1001, 'DATABASE_UNAVAILABLE')
UPDATE_FAILED = ResponseCode(1002, 'UPDATE_FAILED')
BAD_REQUEST = ResponseCode(1003, 'BAD_REQUEST')
