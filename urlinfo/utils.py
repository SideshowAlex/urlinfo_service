from flask import jsonify

from urlinfo.collections import AttrDict as d


def _response_code_output(result, **context):
    return d(result_code=result.code, result=result.id, **context)


def json_resp(result, status_code=200, **context):
    data = _response_code_output(result, **context)
    resp = jsonify(data)
    resp.status_code = status_code
    return resp
