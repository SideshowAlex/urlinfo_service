import logging

from flask import Blueprint, request

import urlinfo.exceptions as exc
from urlinfo import lookup_service
from urlinfo.collections import AttrDict as d
from urlinfo.response_codes import UNKNOWN_ERROR
from urlinfo.utils import json_resp

log = logging.getLogger(__name__)

update = Blueprint('update', __name__)


@update.route('/', methods=['POST'])
def add_urls():
    log.debug(f'Request made to update')

    if not request.is_json:
        raise exc.BadRequestError('Request is not JSON')

    request_json = d(request.get_json())
    if 'urls' not in request_json:
        raise exc.BadRequestError('Request missing "urls" key')

    unexpected_keys = [k for k in request_json if k != 'urls']
    if unexpected_keys:
        raise exc.BadRequestError(f'Unexpected keys found: {unexpected_keys}')

    urls = request_json.urls
    # More checking should be done to ensure that urls is a list of strings..

    try:
        result = lookup_service.add_urls(urls)
        return json_resp(result)
    except exc.ServiceError as e:
        log.error(e)
        raise e
    except Exception as e:
        log.exception(e)
        raise exc.ServiceError(UNKNOWN_ERROR)


@update.errorhandler(exc.BadRequestError)
def handle_bad_request(error):
    context = d()
    if error.msg:
        context.reason = error.msg
    return json_resp(error.response_code, status_code=400, **context)


@update.errorhandler(exc.UpdateFailedError)
def handle_update_failure(error):
    failures = [
        d(url=f.url, result=f.response_code.id,
          result_code=f.response_code.code)
        for f in error.failures
    ]
    context = d(failures=failures)
    if error.msg:
        context.reason = error.msg
    return json_resp(error.response_code, status_code=409, **context)


@update.errorhandler(exc.ServiceError)
def handle_service_error(error):
    context = d()
    if error.msg:
        context.reason = error.msg
    return json_resp(error.response_code, status_code=500, **context)
