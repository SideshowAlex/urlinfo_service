# URL Info Service

We have an HTTP proxy that is scanning traffic looking for malware URLs. Before 
allowing HTTP connections to be made, this proxy asks a service that maintains 
several databases of malware URLs if the resource being requested is known to 
contain malware.

The URL Info Service responds to GET requests where the caller passes in a URL 
and the service responds with some information about that URL. The caller wants 
to know if it is safe to access that URL or not.

## Architecture
There are four architectural layers that comprise the API Lookup service:
* `urlinfo_webapp` - The API frontend. This is realized as replicated docker container running flask. The webapp provided the API to the clients, and is stateless, allowing the service to scale horizontally by adding more instances of this service.
* `haproxy` - A load balancer sitting infront of the `urlinfo_webapp` that
  round-robins requests to the webapps. This allow the application to have a
  single IP. The HAProxy container can be scaled out to remove it as a single
  point of failure. To preserve the single IP, a single DNS record can resolve to the addresses of the proxy instances.
* `lookup_service` - A python module that works on behalf of the 
`urlinfo_webapp` to interface with the  `redis` datastore. The layer is 
stateless, allowing the service to scale horizontally by adding more instances 
of this service.
* `redis` A shared memory data store used store URLs that have been designated 
as malicious. The current deployment configures a single redis instance. For 
additional reliability and performance, a redis cluster can be deployed with no 
change to the application code.


### Data Storage
The data stored in redis is structured as a two-level hash. The first hash is 
the `host_port` of the URL, and the second level is `path` of the URL. Redis 
allows up to $2^{32}$ first level hashes, and $2^{32}$ keys in each of the 
second level hashed, allowing for a vast number of URLs to be stored. 

The port in the `host_port` is removed if the port is the standard HTTP or 
HTTPS port (80 and 443). Any repeated slashes in the `path` part of the URL are 
compressed to that there are no duplicates.

### Unfinished Work
A rabbitmq event queue was attempted to be introduced to separate the
`lookup_service` and `urlinfo_webapp` from the same containers. This allowed a
better distribution of work over multiple container, and to introduce support
for long running task, such as a callback mechanism to the bulk update to inform
the caller the results of the update. There was some initial success, however it
broke the exisiting unit tests.

## API
### Checking is a URL is safe
To determine if a URL is safe, the an HTTP GET can be issued to the endpoint:

    http://api-server/urlinfo/1/{host_port}/{path}{query-string}

A successful call with result in a HTTP OK (200) response, with the results 
contained in JSON payload of the response. A URL that is safe will be indicated 
by the message:

    {"result": "SAFE, "result_code": 101}

A URL  that is identified as malicious is indicated by the message:

    {"result": "UNSAFE, "result_code": 102}
   
If there is an error, a HTTP Server Error (500) with be returned. The message 
body will indicate the cause of the error:

    {"result": "<REASON>", "result_code": <REASON_CODE>}
    
### Marking a URL as unsafe
To mark a URL as unsafe, the an HTTP POST can be issued to the endpoint:

    http://api-server/urlinfo/1/{host_port}/{path}{query-string}
The HTTP POST request is not expected to present. If one is present, it will be 
ignored

A successful call with result in a HTTP OK (200) response, with the results 
contained in JSON payload of the response. A successful call will result in an 
OK response:

    {"result": "OK, "result_code": 100`}

If there is an error, a HTTP Server Error (500) with be returned. The message 
body will indicate the cause of the error:

    {"result": "<REASON>", "result_code": <REASON_CODE>}

### Batch import of URLs
To mark a batch of URs as unsafe, the an HTTP POST can be issued to the endpoint:

    http://api-server/update/1/

The body of the HTTP POST request will supply the URLs to be inserted. THe
format of the URL is as follows

    {
        "urls": [
            "{host_port}/{path}",
            "{host_port}/{path}",
            "{host_port}/{path}",
            ...
        ]
    }


A successful call with result in a HTTP OK (200) response, with the results 
contained in JSON payload of the response. A successful call will result in an 
OK response:

    {"result": "OK, "result_code": 100`}

If there is an error, a HTTP Conflict (409) with be returned. The message 
body will indicate their was an error encounters, and a list of  URLs that 
could not be inserted:

    {
        "result": "UPDATE_FAILED",
        "result_code": 1002,
        "failures": [
            {"url": "{host_port}/{path}", "result": "<REASON>", "result_code": <REASON_CODE>}
            {"url": "{host_port}/{path}", "result": "<REASON>", "result_code": <REASON_CODE>}
            ...
        ]
    }

If the request is malformed an HTTP Bad Request (400) is returned, with an error
message in the JSON body.


## Building and Deploying
### Build
In order to deploy the application, the docker containers must be built first:

    # Define a tag for the container
    $ export TAG=$(date +%Y%m%d.%H%M%S)
    
    # Build and tag the base image
    $ docker build -t base:${TAG} -f docker/Dockerfile-base .
    $ docker image tag base:${TAG} base:latest
    
    # Build and tag the webapp image
    $ docker build -t urlinfo_webapp:${TAG} -f docker/Dockerfile-urlinfo_webapp 
.
    $ docker image tag urlinfo_webapp:${TAG} urlinfo_webapp:latest

### Deploy
Once build, the service can be deployed with docker swarm. Please refer to 
docker's documentation for configuring your swam cluster. 
To deploy the URL Info Service into the cluster, run the following:

    $ docker stack deploy --compose-file=docker-compose.yml urlinfo_service

## Testing
Unit test that exercise functionality, code coverage and tests for PEP-8 can be 
run using tox. To setup a developer's environment, you will require Python 3.7. 
Once installed, run the following to install tox:

    $ pip install -r dev-requirement.txt
   
Tests can be run using the command:

    # To run all tests
    $ tox
    
    # To run only the unit-tests
    $ tox -epy37
    
    # To run only pep8 compliance tests
    $ tox -epep8
    
    # To run only import ordering compliance tests
    $ tox -eisort
    
    # To run only coverage tests
    $ tox -ecoverage
    

   
`` 
